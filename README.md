# GoLang & TinyGo Template

- Go `1.13`
- TinyGo `0.19.0`

## Checklist

- Update `go.mod`

```bash
tinygo build -o main.wasm -target wasm ./main.go
```